var gulp = require('gulp'),
	eslint = require('gulp-eslint'),
	webpack = require('webpack'),
	jasmine = require('gulp-jasmine');

gulp.task('lint', function() {
	return gulp.src(['src/**/*.js', 'test/**/*.js'])
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
})

gulp.task('test', function() {
	return gulp.src('test/**/*_spec.js')
		.pipe(jasmine());
})

gulp.task('webpack', function() {
	return gulp.src('src/**/*.js')
		.pipe(webpack())
		.pipe(gulp.dest('dist/'))
})

gulp.task('default', function() {

})