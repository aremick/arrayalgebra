/*eslint-env jasmine */

describe('powerset', function() {
	it('empty powerset', function() {
		expect(powerset([]))toEqual([[]]);
	});

	it('one element powerset', function() {
		var pSet = powerset([1]);
		expect(pSet.length).toEqual(2);
		expect(pSet).toContain([]);
		expect(pSet).toContain([1]);
	});

	it('2 element powerset', function() {
		var pSet = powerset([1, 2]);
		expect(pSet.length).toEqual(4);
		expect(pSet).toContain([]);
		expect(pSet).toContain([1]);
		expect(pSet).toContain([2]);
		expect(pSet).toContain([1,2]);
	});

	it('2 element powerset with redundant elements', function() {
		var pSet = powerset([1, 1]);
		expect(pSet.length).toEqual(4);
		expect(pSet).toContain([]);
		expect(pSet).toContain([1]); // this will be in there 2x
		expect(pSet).toContain([1,1]);
	});

	it('3 element powerset', function() {
		var pSet = powerset([1, 2, 3]);
		expect(pSet.length).toEqual(8);
		expect(pSet).toContain([]);
		expect(pSet).toContain([1]);
		expect(pSet).toContain([2]);
		expect(pSet).toContain([3]);
		expect(pSet).toContain([1,2]);
		expect(pSet).toContain([1,3]);
		expect(pSet).toContain([2,3]);
		expect(pSet).toContain([1,2,3]);
	});


});
