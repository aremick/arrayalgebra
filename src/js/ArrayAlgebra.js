/* eslint-env amd */

define('array_algebra', function arrayAlgebra() {
    'use strict';

    var subtract = -1;

    /**
     * Create an object with keys for each item in the array and a value that
     * is
     * @param  {[type]} array [description]
     * @param  {[type]} value [description]
     * @param  {[type]} object   [description]
     * @return {[type]}       [description]
     */
    function objectify(array, value, object) {
        var i = 0,
            obj = object || {},
            val = value || 1;

        for (i = array.length - 1; i >= 0; i--) {
            if (typeof obj[array[i]] === 'undefined') {
                obj[array[i]] = 0;
            }
            obj[array[i]] += val;
        }
        return obj;
    }

    /**
     * create a new array with all elements from array but no duplicates
     * @param  {*[]} array array of things
     * @return {*[]} array of unique things
     */
    function setify(array) {
        var i = 0,
            obj = objectify(array, 1),
            set = [];

        for (i in obj) {
            if (obj.hasOwnProperty(i)) {
                set.push(i);
            }
        }
        return set;
    }

    /**
     * create a new array containing all the elements of both arrays.
     * @param  {[type]} array1 [description]
     * @param  {[type]} array2 [description]
     * @return {[type]}        [description]
     */
    function union(array1, array2) {
        return array1.concat(array2);
    }

    /**
     * find all elements that exist in both arrays
     * @param  {[type]} array1 [description]
     * @param  {[type]} array2 [description]
     * @return {[type]}        [description]
     */
    function intersection(array1, array2) {
        var element = 0,
            i = 0,
            obj = objectify(array1),
            result = [];

        for (i = array2.length - 1; i >= 0; i--) {
            element = array2[i];
            if (obj[element] && obj[element] > 0) {
                result.push(element);
                obj[element]--;
            }
        }

        return result;
    }

    /**
     * Creates a new array containing the difference of the two arrays.
     * @param  {*[]} array1 - array of things
     * @param  {*[]} array2 - array of things
     * @param  {Boolean} [symmetric=false] - If false, the elements of
     * array1 minus array 2 will be returned. If true, the elements of both
     * arrays not present in the other will be returned.
     * @return {*[]} a new array containing the difference of the two arrays.
     */
    function difference(array1, array2, symmetric) {
        var count = 0,
            i = 0,
            j = 0,
            obj = objectify(array1),
            result = [];

        objectify(array2, subtract, obj);
        for (i in obj) {
            if (obj.hasOwnProperty(i)) {
                count = symmetric ? Math.abs(obj[i]) : obj[i];
                for (j = 0; j < count; j++) {
                    result.push(i);
                }
            }
        }
        return result;
    }

    /**
     * generate all the possible 2 elememt arrays from the param arrays
     * @param  {[type]} array1 [description]
     * @param  {[type]} array2 [description]
     * @return {[type]}        [description]
     */
    function cartesianProduct(array1, array2) {
        var i, j,
            result = [];

        for (i = array1.length - 1; i >= 0; i--) {
            for (j = array2.length - 1; j >= 0; j--) {
                result.push([array1[i], array2[j]]);
            }
        }
        return result;
    }

    /**
     * [powerSet description]
     * @param  {[type]} array [description]
     * @return {[type]}       [description]
     */
    function powerSet(array) {
        var i,
            privateArray = array.slice(),
            result = [],
            shiftVal = privateArray.shift(),
            subSet = powerSet(privateArray);

        for (i = subSet.length - 1; i >= 0; i--) {
            result.push(subSet[i].push(shiftVal));
        }
        return result.concat(subSet);
    }

    /**
     * [isSubset description]
     * @param  {[type]}  array1 [description]
     * @param  {[type]}  array2 [description]
     * @return {Boolean}        [description]
     */
    function isSubset(array1, array2) {
        var i,
            obj = objectify(array1);

        objectify(array2, subtract, obj);
        for (i in obj) {
            if (obj.hasOwnProperty(i) && obj[i] < 0) {
                return false;
            }
        }
        return true;
    }

    return {
        setify: setify,
        union: union,
        intersection: intersection,
        difference: difference,
        cartesianProduct: cartesianProduct,
        powerSet: powerSet,
        isSubset: isSubset
    };
});
